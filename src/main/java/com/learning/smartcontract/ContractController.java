package com.learning.smartcontract;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@RestController
@Slf4j
public class ContractController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    WebClient.Builder webClientBuilder;

    @GetMapping("/blockchain-service")
    String getBlockChainConfig() {
        return restTemplate.exchange(
                "http://blockchain-service/hello?name=Nhat",
                HttpMethod.GET,
                null,
                String.class).getBody();
    }

    @GetMapping("/call-with-rx")
    List<Singer> rxCalling() {
        return webClientBuilder.build()
                .get()
                .uri("http://blockchain-service/singers")
                .retrieve()
                .bodyToFlux(Singer.class)
                .collectList()
                .block();
    }
}
